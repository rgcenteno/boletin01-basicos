/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin1;

import java.util.Scanner;
/**
 *
 * @author rgcenteno
 */
public class Boletin1_07 {
    
    public static void calculosCirculo(){
        Scanner teclado = new Scanner(System.in);
        System.out.println("Por favor escriba el radio.");
        double radio = teclado.nextDouble();
                
        System.out.println("El área del circulo es: " + calcularArea(radio) + " y su perímetro es " + calcularPerimetro(radio));
    }
    
    public static double calcularArea(double r){
        double area = r * r * Math.PI;
        return area;
    }
    
    public static double calcularPerimetro(double radio){
        double area = 2 * Math.PI * radio;
        return area;
    }
    
    
}
