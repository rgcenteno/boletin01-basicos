/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin1;

import java.util.Scanner;

/**
 *
 * @author rgcenteno
 */
public class Boletin1_09 {
    
    public static void mostrarCDU(){
        Scanner teclado = new Scanner(System.in);
        int numero = teclado.nextInt();
        System.out.println(solucionSencilla(numero));
        System.out.println(solucionInt(numero));
    }
    
    public static String solucionSencilla(int valor){        
        String convertido = valor + "";
        char centenas = convertido.charAt(0);
        char decenas = convertido.charAt(1);
        char unidades = convertido.charAt(2);
        String resultado = "Centenas: " + centenas + "\nDecenas: " + decenas + "\nUnidades: "+ unidades;
        return resultado;
    }
    
    public static String solucionInt(int valor){
        int centenas = valor / 100;
        int resto = valor % 100;
        int decenas = resto / 10;
        int unidades = valor - (centenas * 100 + decenas * 10);
        String resultado = "Centenas: " + centenas + "\nDecenas: " + decenas + "\nUnidades: "+ unidades;
        return resultado;
    }
    
}
