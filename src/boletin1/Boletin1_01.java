/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin1;

import java.util.Scanner;

/**
 * Mostrar pull
 * @author rgcenteno
 */
public class Boletin1_01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //calcularCuadrado();
        //Boletin1_03.facturaHotel();
        //Boletin1_07.calculosCirculo();
        //Boletin1_08.kmhMs();
        Boletin1_09.mostrarCDU();
    }
    
    public static void calcularCuadrado(){
        System.out.println("Por favor, inserte un número.");
        Scanner teclado = new Scanner(System.in);
        double numero = teclado.nextDouble();        
        System.out.printf("Resultado: %.2f\n", numero*numero);
        
    }
    
    public static double calcularAreaRectangulo(double base, double altura){
        double area = base * altura;
        return area;
    }
    
}
