/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin1;

import java.util.Scanner;

/**
 *
 * @author rgcenteno
 */
public class Boletin1_06 {
    
    public static void mostrarDatos(){
        Scanner teclado = new Scanner(System.in);
        System.out.println("Por favor, inserte su nombre y apellidos.");
        String nombreApellidos = teclado.nextLine();
        System.out.println("Por favor, inserte su edad.");
        int edad = teclado.nextInt();
        System.out.println("Por favor, inserte la nota media.");
        double notaMedia = teclado.nextDouble();
        System.out.printf("El alumno %s, de %d años de edad tiene una nota media de %.2f", nombreApellidos, edad, notaMedia);
    }
}
