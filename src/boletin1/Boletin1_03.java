/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin1;

/**
 *
 * @author rgcenteno
 */
public class Boletin1_03 {
    
    
    public static void facturaHotel(){
        java.util.Scanner teclado = new java.util.Scanner(System.in);
        System.out.println("Inserte el precio por noche en TA");
        double precioTA = teclado.nextDouble();
        System.out.println("Inserte el número de noches en TA");
        int nochesTA = teclado.nextInt();
        
        System.out.println("Inserte el precio por noche en TB");
        double precioTB = teclado.nextDouble();
        System.out.println("Inserte el número de noches en TB");
        int nochesTB = teclado.nextInt();
        
        double total = (precioTA * nochesTA) + (precioTB * nochesTB);
        double porcentajeTA = (precioTA * nochesTA) / total;
        double porcentajeTB = 100 - porcentajeTA;
        
        System.out.printf("Total: %.2f.\n PorcentajeTA: %.2f\n PorcentajeTB: %.2f", total, porcentajeTA, porcentajeTB);
    }
}
