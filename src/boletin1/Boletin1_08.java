/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin1;

import java.util.Scanner;

/**
 *
 * @author rgcenteno
 */
public class Boletin1_08 {    
    
    public static void kmhMs(){
        Scanner teclado = new Scanner(System.in);
        System.out.println("Por favor, inserta los Km/h");
        int kmh = teclado.nextInt();
        System.out.println(kmh + "  Km/h equivalen a " + kmhToMs(kmh));
        //System.out.printf("%d Km/h equivalen a %.2fm/s\n", kmh, kmhToMs(kmh));
    }
    
    public static double kmhToMs(int kmh){
        final double FACTOR_CONVERSION = 0.277778;
        return kmh * FACTOR_CONVERSION;
    }
}
