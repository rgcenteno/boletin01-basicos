/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin1;

/**
 *
 * @author rgcenteno
 */
public class Boletin1_02 {
    
    public static void calcularIngresosMes(){
        System.out.println("Por favor, inserte el coste por hora.");
        java.util.Scanner teclado = new java.util.Scanner(System.in);
        double precioHora = teclado.nextDouble();
        System.out.println("Por favor, inserte las horas trabajadas.");
        double horasTrabajadas = teclado.nextDouble();
        double importeTotal = horasTrabajadas * precioHora;
        System.out.printf("Importe total: %.2f", importeTotal);
    }
}
